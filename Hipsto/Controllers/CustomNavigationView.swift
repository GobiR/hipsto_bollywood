//
//  CustomNavigationView.swift
//  Hipsto
//
//  Created by Gobi R. on 28/05/19.
//  Copyright © 2019 Dci. All rights reserved.
//

import UIKit

@IBDesignable
class CustomViewWithXib: UIView {
  
   @IBInspectable var nibName:String?
    var contentView:UIView?
    
    required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
      commonInit()
    }
    
    override init(frame: CGRect) {
      super.init(frame: frame)
      commonInit()
    }
    
    func commonInit() {
      guard let view = loadViewFromNib() else { return }
      view.frame = self.bounds
      view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
      self.addSubview(view)
      contentView = view
    }
    
    func loadViewFromNib() -> UIView? {
      guard let nibName = nibName else { return nil }
      let bundle = Bundle(for: type(of: self))
      let nib = UINib(nibName: nibName, bundle: bundle)
      return nib.instantiate(withOwner: self, options: nil).first as? UIView
    }
  override func prepareForInterfaceBuilder() {
    super.prepareForInterfaceBuilder()
    commonInit()
    contentView?.prepareForInterfaceBuilder()
  }
}
